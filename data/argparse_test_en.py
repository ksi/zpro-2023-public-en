import argparse

# Creation of parser
parser = argparse.ArgumentParser(description='Program for calculating sum and difference.')

# Adding arguments
# positional arguments
parser.add_argument('operand1', type=int, help='Left operand')
parser.add_argument('operand2', type=int, help='Right operand')

# optional arguments with arbitrary placement
parser.add_argument('-a', '--addition', action='store_true', help='Adds both operands')
parser.add_argument('-s', '--subtraction', action='store_true', help='Subtracts right operand from the left operand')

# parsing command line arguments
args = parser.parse_args()

# Calculation on values of passed arguments
if args.addition:
    result = args.operand1 + args.operand2
    print(f'Sum: {result}')
if args.subtraction:
    result = args.operand1 - args.operand2
    print(f'Difference: {result}')
if not args.addition and not args.subtraction:
    print('No operation was specified.')
