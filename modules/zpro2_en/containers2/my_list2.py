__all__ = ['new_list2']

from . import dictionary2  # import from paralel module in this package
from ..files2 import writing2  # import from the paralel subpackage
from ..generic2  import write_test2   # import from parent package

# module for working with lists
def new_list2():
    print('function for creating new list')

def reverse_list2():
    print('function for reversing list')

def use_relative_import():
    dictionary2.new_dictionary2()
    writing2.write_text2()
    write_test2()
    