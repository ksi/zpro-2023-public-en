

myvariable = f'I am variable myvariable from module {__name__}'

def say_hello():
    print(f'Hello! I am function say_hello from module {__name__} ')

print(f'This is an executable statement from module {__name__}')

if __name__ == '__main__':
    print('This part will be executed only when launching as a script.')
    print('Here we can run functions from module such as say_hello')
    say_hello()