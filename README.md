# ZPRO 2023

This page will contain all information related to the **English** version of 18ZPRO – announcements, course materials, etc.

The contents of this project can be easily synchronized into the [JupyterHub](https://jlk.fjfi.cvut.cz/jupyter/) environment by clicking on the following button:

[![Static Badge](https://img.shields.io/badge/Synchronize%20to%20JupyterHub-08c?logo=jupyter&labelColor=white&color=08c)](
https://jlk.fjfi.cvut.cz/jupyter/hub/user-redirect/git-pull?repo=https%3A%2F%2Fgitlab.fjfi.cvut.cz%2Fksi%2Fzpro-2023-public-en.git&urlpath=lab%2Ftree%2Fzpro-2023-public-en.git%2FREADME.md&branch=main)


## Information regarding the course

We will meet every Monday at 14:00 in T-105 and every Thursday at 10:00 in T-124. Presence is not compulsory, but is highly recommended. The evaluation of your progress through the course will be based on home assignments (12 per term) and two quick (15 minutes) test. If you manage to get at least 90- points from both home assignments and tests, you will receive the "zápočet" and credits. If you receive at least 50% of points both from home assignments and test, you will have to implement and defend semestral project (mor details about the project will be published during December). In case you will receive less than 50% of points from home assignments or test, then the requirements to close the course will be discussed individually.

In the context of the Introduction to programming course, we consider using AI (Chatbot, copilot) as a cooperation with human. You should solve the home assignments on your own. You can use AI for help, but not for solving the assignments. During the test, use of AI and coopoeration with someone else will not be permitted.

## Announcements

Lesson on 28th September is cancelled due to bank holidays in Czech republic

## Links

- ["White Book"](https://bilakniha.cvut.cz/en/predmet11274505.html) – study plans, supplementary literature
- [GitLab project](https://gitlab.fjfi.cvut.cz/ksi/zpro-2023-public-en)
- [Introductory lecture (in Czech)](https://gitlab.fjfi.cvut.cz/ksi/zpro-2023-public)
- [Information about FJFI user account](https://it.fjfi.cvut.cz/en/clanky/user-account)
- [FJFI JupyterHub](https://jlk.fjfi.cvut.cz/jupyter/)
- [Anonymous Jupyter environment](https://jupyter.org/try-jupyter)

## Authors

The materials for the English variant of the course were prepared by Vladimír Jarý.

The original [Czech materials](https://gitlab.fjfi.cvut.cz/ksi/zpro-2023-public) were prepared by:

- Jakub Klinkovský
- Zuzana Petříčková
- Vladimír Jarý
- Maksym Dreval
- Petr Pauš
- Jan Tomsa
- František Voldřich

The materials are available under the [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) license.
